# Electrónica

El sistema de control de la termoformadora se compone de dos partes: la [interfaz de usuario](#interfaz-de-usuario) que permite seleccionar y monitorear la temperatura de los calefactores y el [control de temperatura de los calefactores](#control-de-temperatura-calefactores).

### Listado de componentes

| Item | Cantidad | Link de referencia  |
| -- | --- |  --- |
| Arduino UNO | 1 |
| Pantalla tactil 3.5''   | 1 |
| Módulo Max6675 |  1 |
| Termocupla tipo K | 1 |
| Conversor lógico 4 canales |  2 |
| Relé de estado sólido AC 25A  | 1 |
| Calefactor cerámico infrarrojo 400W rectangular | 4 |
| Calefactor cerámico infrarrojo 400W cuadradado con termocupla |  1 |
| Fuente AC-DC 12v |  1 |
| Regleta de distribución 10A  | 2  |
| Regleta distribución cerámica 40A | 4 |
| Conector GX16-4P  | 2 |
| Conector GX16-2P  | 1 |


## Interfaz de usuario
La interfaz de usuario es una pantalla táctil de 3.5'' con driver ILI9488. La documentación técnica de la pantalla la puedes encontrar en el siguiente
[link](http://www.lcdwiki.com/3.5inch_SPI_Module_ILI9488_SKU:MSP3520).

La lógica de la pantalla es de 3.3v, por lo que es necesario utilizar un adapator de niveles para cambiar de 5v de los pines del Arduino UNO a 3.3v de la pantalla.

<img src="img/level-shifter-spi-2-500x500.png" width="300">

La conexión para los 14 pines de la pantalla es la siguiente:

| # | Arduino | Pantalla |  Nivel Lógico  |
| - | -----   | -------- |  ------ |
| 1 |   5V    | VCC         | -       |
| 2 |   GND   | GND         | -       |
| 3 |   A5    | CS          | 3.3v    |
| 4 |   RST   | Reset       | -       |
| 5 |   A3    | DC/RS/CD    | 3.3v    |
| 6 |   D11   | SDI/WR      | 3.3v    |
| 7 |   D13   | SCK         | 3.3v    |
| 8 |   3.3v  | LED         | -       |
| 9 |   NC    | SDO         | -       |
| 10|   D3    | T_CLK       | 3.3v    |
| 11|   D2    | T_CS        | 3.3v    |
| 12|   D5    | T_DIN       | 3.3v    |
| 13|   D4    | T_DO        | 3.3v    |
| 14|   D6    | T_IRQ       | 5v      |

Diagrama de conexiones eléctricas entre el Arduino, pantalla táctil y módulo MAX6675
![Diargama eléctrico DC](img/diagrama_termo_dc.png)

El código de la interfaz y control de temperatura de la termoformadora lo puedes encontrar [acá](src/thermo_screen/thermo_screen.ino).
Para probar las conexiones de la pantalla tenemos un  [código de prueba](src/graphicstest/graphicstest.ino). Para compilar el código de prueba y el código de la pantalla se necesitan las librerías:

* [LCDWIKI_SPI](https://github.com/lcdwiki/LCDWIKI_SPI)
* [LCDWIKI_GUI](https://github.com/lcdwiki/LCDWIKI_gui)
* [LCDWIKI_TOUCH](https://github.com/lcdwiki/LCDWIKI_touch)
* [LCDWIKI_KVB](https://github.com/lcdwiki/LCDWIKI_kbv)
* [MAX6675](https://github.com/adafruit/MAX6675-library)

## Control de temperatura calefactores

La termoformadora utiliza 5 calefactores cerámicos de 400W de corriente alterna y se controla su temperatura utilizando un controlador PID discreto, utilizando un relé de estado sólido para encender o apagar los calefactores durante un tiempo determinado por el controlador. Para medir la temperatura del sistema se utiliza una termocupla tipo K integrada en el calefactor del centro, con un amplificador MAX6675.

Para conectar los 5 calefactores cerámicos infrarrojos de la termoformadora es necesario utilizar regletas **cerámicas** y no plásticas, ya que debido a la cercanía de los calefactores con las regletas las plásticas podrían derretirse.

El diagama de corriente alterna de la termoformadora es el siguiente:
![Diargama eléctrico parte AC](img/diagrama_termo_ac.png)

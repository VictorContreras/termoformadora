# Partes, piezas y componentes

## Partes cortadas en laser y plegadas

En [esta](cad/corte-laser-plegado) carpeta del  repositorio puedes encontrar los archivos para el corte y plegado de las piezas que conforman la estructura de la termoformadora y la bomba de vacío.

| Item | Cantidad | Material | Espesor [mm] | Link de referencia  |
| -- | --- |  --- | ---  | ---  |
| Pieza 01  | 2 | Acero Inoxidable  | 1.5 |
| Pieza 02  | 1 | Acero Inoxidable  | 1.5 |
| Pieza 03  | 1 | Acero Inoxidable  | 1.5 |
| Pieza 04  | 1 | Aluminio  | 4 |
| Pieza 05  | 1 | Aluminio  | 4 |
| Pieza 06  | 2 | Acero Inoxidable  | 0.8 |
| Pieza 07  | 2 | Acero Inoxidable  | 0.8 |
| Pieza 08  | 1 | Acero Inoxidable  | 2 |
| Pieza 09  | 1 | Acero Inoxidable  | 2 |
| Pieza 10  | 1 | Acero Inoxidable  | 2 |
| Pieza 11  | 1 | Acero Inoxidable  | 1.5 |
| Pieza 12  | 1 | Acero Inoxidable  | 1.5 |
| Pieza 13  | 1 | Acero  | 2 |
| Pieza 14  | 1 | Acero  | 2 |


## Partes comerciales

### Sistema electrico
| Item | Cantidad | Link de referencia  |
| -- | --- |  --- |
| Arduino UNO | 1 |
| Pantalla tactil 3.5''   | 1 |
| Módulo Max6675 |  1 |
| Termocupla tipo K | 1 |
| Conversor lógico 4 canales |  2 |
| Relé de estado sólido AC 25A  | 1 |
| Calefactor cerámico infrarrojo 400W rectangular | 4 |
| Calefactor cerámico infrarrojo 400W cuadradado con termocupla |  1 |
| Fuente AC-DC 12v |  1 |
| Regleta de distribución 10A  | 2  |
| Regleta distribución cerámica 40A | 4 |
| Conector GX16-4P  | 2 |
| Conector GX16-2P  | 1 |

### Sistema de vacío y vaporizado
| Item | Cantidad | Link de referencia  |
| -- | --- |  --- |
| Bomba de vacío | 1 |
| Vaporizador  | 1 |

## Partes impresas

| Item | Cantidad | Enlace al archivo  |
| -- | --- |  --- |
| Manilla A | 2 | [manilla-a.stl](cad/manillas/manilla-a.stl)
| Manilla B | 2 | [manilla-b.stl](cad/manillas/manilla-b.stl)
| Copla liberador de vacío |  1 | [copla.stl](cad/liberador-de-vacio/copla.stl)
| Housing rodamiento liberador de vacío | 4 | [houing-rodamiento.stl](cad/liberador-de-vacio/housing-rodamiento.stl)
| Termocupla tipo K | 1 | [palanca.stl](cad/liberador-de-vacio/palanca.stl)
| Fijación ejes parte 1 |  1 | [fijacion-1.stl](cad/fijacion-ejes/fijacion-1.stl)
| Fijación ejes parte 2 |  1 | [fijacion-2.stl](cad/fijacion-ejes/fijacion-2.stl)

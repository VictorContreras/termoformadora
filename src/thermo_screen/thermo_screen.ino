/* codigo para pantalla touch ILI9488 3,5" 480x320px
 * por Gonzalo Olave para Nodo de Biofabricacion
 *
 * (C) FabLab U. de Chile
 */

// IMPORTANT: LCDWIKI_SPI LIBRARY AND LCDWIKI_TOUCH LIBRARY MUST BE SPECIFICALLY
// CONFIGURED FOR EITHER THE TFT SHIELD OR THE BREAKOUT BOARD.

//when using the BREAKOUT BOARD only and using these hardware spi lines to the LCD,
//the SDA pin and SCK pin is defined by the system and can't be modified.
//if you don't need to control the LED pin,you can set it to 3.3V and set the pin definition to -1.
//other pins can be defined by youself,for example
//pin usage as follow:
//             CS  DC/RS  RESET  SDI/MOSI  SDO/MISO  SCK  LED    VCC     GND
//Arduino Uno  A5   A3     A4      11        12      13   A0   5V/3.3V   GND
//            T_IRQ  T_DO  T_DIN  T_CS  T_CLK
//Arduino Uno  6      4      5     2      3

//Remember to set the pins to suit your display module!

#define SERIAL_DEBUG false

#include "max6675.h"

#include <LCDWIKI_GUI.h> //Core graphics library
#include <LCDWIKI_SPI.h> //Hardware-specific library
#include <LCDWIKI_TOUCH.h> //touch screen library

//paramters define
#define MODEL ILI9488_18
#define CS   A5
#define CD   A3
#define RST  A4
#define LED  A0   //if you don't need to control the LED pin,you should set it to -1 and set it to 3.3V

//touch screen paramters define
#define TCS   2
#define TCLK  3
#define TDOUT 4
#define TDIN  5
#define TIRQ  6

//the definiens of hardware spi mode as follow:
//if the IC model is known or the modules is unreadable,you can use this constructed function
LCDWIKI_SPI my_lcd(MODEL,CS,CD,RST,LED); //model,cs,dc,reset,led

//the definiens of touch mode as follow:
LCDWIKI_TOUCH my_touch(TCS,TCLK,TDOUT,TDIN,TIRQ); //tcs,tclk,tdout,tdin,tirq

#define fillRoundRect(X,Y,W,H) Fill_Round_Rectangle(X,Y,W,H,5)
#define setColor(R,G,B) Set_Draw_color(R,G,B)
#define setTextSize(S) Set_Text_Size(S)
#define setTextColor(C) Set_Text_colour(C)

// Assign human-readable names to some common 16-bit color values:
#define BLACK       0x0000
#define BLUE        0x001F
#define RED         0xF800
#define GREEN       0x07E0
#define CYAN        0x07FF
#define MAGENTA     0xF81F
#define YELLOW      0xFFE0
#define WHITE       0xFFFF
#define ORANGE      0xFD20
#define GREENYELLOW 0xAFE5
#define NAVY        0x000F
#define DARKGREEN   0x03E0
#define DARKCYAN    0x03EF
#define MAROON      0x7800
#define PURPLE      0x780F
#define OLIVE       0x7BE0
#define LIGHTGREY   0xC618
#define DARKGREY    0x7BEF

#define GRAY_1 60
#define VPW_MAG     178,82,61
#define VPW_BLU     102,161,210
#define VPW_PUR     44,4,82
#define VPW_GRAY    69,95,101
#define VPW_CLEAR   220,213,213
#define VPW_MBLU    Color_To_565(44,53,82) //44,53,82

#define W 320
#define H 480
#define MARGIN 10 // px
#define SEP 5
#define TZ_W (W-(2*MARGIN))
#define TZ_H ((H-2*MARGIN)/2)
#define B_W ((TZ_W/2)-SEP)
#define B_H ((TZ_H/3)-SEP)
#define BB_W (TZ_W/2)
#define BB_H (TZ_H/2)

// boton 1
#define B1_X MARGIN
#define B1_Y MARGIN+TZ_H
#define B1_W B1_X + B_W
#define B1_H B1_Y + B_H
#define B1_TXTX (B1_W + B1_X)/2-MARGIN/2
#define B1_TXTY (B1_H + B1_Y)/2-MARGIN/2
// boton 2
#define B2_X MARGIN
#define B2_Y B1_H+SEP
#define B2_W B2_X+B_W
#define B2_H B2_Y+B_H
#define B2_TXTX (B2_W + B2_X)/2-(3*MARGIN)
#define B2_TXTY (B2_H + B2_Y)/2
// boton 3
#define B3_X MARGIN
#define B3_Y B2_H+SEP
#define B3_W B3_X+B_W
#define B3_H B3_Y+B_H
#define B3_TXTX (B3_W + B3_X)/2
#define B3_TXTY (B3_H + B3_Y)/2
// boton 4
#define B4_X W-MARGIN-BB_W
#define B4_Y MARGIN+TZ_H
#define B4_W B4_X+BB_W
#define B4_H B4_Y+BB_H
#define B4_TXTX B4_X + MARGIN*3
#define B4_TXTY B4_Y + MARGIN*5
// boton 5
#define B5_X W-MARGIN-BB_W
#define B5_Y B4_H+SEP
#define B5_W B5_X+BB_W
#define B5_H B5_Y+BB_H
#define B5_TXTX B5_X + MARGIN*4
#define B5_TXTY B5_Y + MARGIN*5

// zona de texto
#define TZ_X MARGIN //(10,10)
#define TZ_Y MARGIN
#define TZ_WP TZ_W
#define TZ_HP TZ_H

#define REF_X  TZ_X+MARGIN
#define REF_Y  TZ_Y+MARGIN*8
#define TEMP_X TZ_X+MARGIN
#define TEMP_Y TZ_Y+MARGIN*12
#define STATUS_X TZ_X+MARGIN
#define STATUS_Y TZ_X+MARGIN*16
#define DELTA_X 7*MARGIN
#define DELTA_Y 0

#define PRESSED 1
#define NOT_PRESSED 0
bool _press = NOT_PRESSED;

int X, Y;
bool b1_flag, b2_flag, b3_flag, b4_flag, b5_flag;

#define TEMP    1 // paso de 50
#define DTEMP   2 // paso de 10
#define DDTEMP  3 // paso de 1

int tref = 100;
int dtemp = 50;
int ddtemp = 10;
int dddtemp = 1;

int mode = 1;

int en_st = 0;

//Sensor
float temperatura = 0;
int sr = 200; // sample rate
#define TC_SCK      10
#define TC_CS       9
#define TC_DO       8
MAX6675 thermocouple(TC_SCK,TC_CS, TC_DO);
unsigned long tiempo = 0; // contador de tiempo

//Rele
#define PINRELE A0
#define ON      1
unsigned long trele = 0;

//PID
float error = 0;
float pe = 0;
float ie = 0;
float de = 0;
#define kp 0.05
#define ki 0.001
#define kd 0.002
float preverror = 0;
float target = 0;
boolean pidon = false;
boolean srele = false;
float dutycycle = 0;

unsigned long dt = 0;
#define T_UPDATE 2000

bool readResistiveTouch(void)
{
  my_touch.TP_Scan(0);
  if (my_touch.TP_Get_State()&TP_PRES_DOWN)
  {
    if(my_touch.x > 0 && my_touch.x < W) X = map(my_touch.x,0,W,W,0); // invierte valores
    if(my_touch.y > 0 && my_touch.y < H) Y = my_touch.y;
    //Serial.print("X: ");Serial.print(X);Serial.print("\tY: ");Serial.println(Y);
    return PRESSED;
  }
  return NOT_PRESSED;
}

void setup() {
  Serial.begin(9600);
  pinMode(PINRELE, OUTPUT);

  my_lcd.Init_LCD();
  my_lcd.Set_Rotation(0);
  my_touch.TP_Set_Rotation(0);
  my_touch.TP_Init(my_lcd.Get_Rotation(),my_lcd.Get_Display_Width(),my_lcd.Get_Display_Height());
  my_lcd.Fill_Screen(BLACK);

  //my_lcd.Fill_Circle(W/3, H/3, 30);
  dibujarMenu();
}

void loop() {

  // actualiza info de la pantalla
  if (millis() - dt > T_UPDATE) {
    #if SERIAL_DEBUG == true
    Serial.print("Ref: ");Serial.print(target);Serial.print("\tTemp: ");Serial.println(temperatura);
    #endif
    updateTemp();
    dt = millis();
  }

  // selecciona la accion segun el boton apretado
  int sel=0;
  if(readResistiveTouch() == PRESSED){
    sel = getSelection();
  }

  if (sel == 1 && b1_flag == HIGH) {
    touchB1();
    b1_flag = LOW;
    updateTRef();
  }
  if (sel == 2 && b2_flag == HIGH) {
    touchB2();
    b2_flag = LOW;
    updateMode();
  }
  if (sel == 3 && b3_flag == HIGH) {
    touchB3();
    b3_flag = LOW;
    updateTRef();
  }
  if (sel == 4 && b4_flag == HIGH) {
    touchB4();
    //Serial.print("M_EN: ");
    //Serial.println(en_st);
    b4_flag = LOW;
    updateTemp();
  }
  if (sel == 5 && b5_flag == HIGH) {
    touchB5();
    //Serial.print("M_EN: ");
    //Serial.println(en_st);
    b5_flag = LOW;
    updateTemp();
  }

  // control de temperatura
  target = tref*1.0;
  pidon = en_st;
  if (millis() - tiempo >= sr) {
    temperatura = thermocouple.readCelsius();

    if (pidon == true) {
      error = target - temperatura;
      pe = kp * error;
      de = kd * (error - preverror) / (sr / 1000);
      preverror = error;
      if (-10.0 < error && error < 10.0) { //el integrativo funciona en el rango+-10
        ie = ie + error * float(1.0 * sr / 1000);
      }
      else {
        ie = 0;
      }
      dutycycle = pe + ie * ki; // tiempo ON en la onda PWM
      dutycycle = sr * constrain(dutycycle, 0, 1); //(normalizado)
    }
    tiempo = millis();
  }

  if (pidon == true) {
    if (millis() - trele < dutycycle) {
      srele = true;
    }
    if (millis() - trele >= dutycycle) {
      srele = false;
    }
    if (millis() - trele >= sr) {
      trele = millis();
    }
  }

  else {
    srele = false;
  }

  if(en_st == ON){
    digitalWrite(PINRELE, srele);
  }
  else{
    digitalWrite(PINRELE, LOW);
  }

}


int getSelection() {

  int sel = 0;
  // boton 1
  if ((X > B1_X && X < B1_W ) && (Y > B1_Y && Y < B1_H ) && b1_flag == LOW) {
    b1_flag = HIGH;
    #if SERIAL_DEBUG == true
    Serial.print("B1 pressed.. ");
    #endif
    //Serial.print("\tz: ");
    //Serial.println(Z);
    // cambia de color el boton
    //my_lcd.fillRoundRect(B1_X, B1_Y, B1_W, B1_H);
    my_lcd.setTextSize(3);
    my_lcd.Set_Text_Back_colour(178,82,161);
    my_lcd.setTextColor(BLACK);
    my_lcd.Print_String("+", B1_TXTX, B1_TXTY);
    while (readResistiveTouch() == PRESSED) {

    }
    // vuelve al color original
    //my_lcd.fillRoundRect(B1_X, B1_Y, B1_W, B1_H);
    my_lcd.setTextSize(3);
    my_lcd.setTextColor(WHITE);
    my_lcd.Print_String("+", B1_TXTX, B1_TXTY);
    #if SERIAL_DEBUG == true
    Serial.println("..B1 released");
    #endif
    sel = 1;
  }

  if ((X > B2_X && X < B2_W ) && (Y > B2_Y && Y < B2_H ) && b2_flag == LOW ) {
    b2_flag = HIGH;
    #if SERIAL_DEBUG == true
    Serial.print("B2 pressed.. ");
    #endif
    //my_lcd.setColor(0, 0, 255);
    //my_lcd.fillRoundRect(B2_X, B2_Y, B2_W, B2_H);
    my_lcd.setTextSize(3);
    my_lcd.setTextColor(BLACK);
    my_lcd.Set_Text_Back_colour(44,4,82);
    my_lcd.Print_String("MODE", B2_TXTX, B2_TXTY);
    while (readResistiveTouch() == PRESSED) {

    }
    //my_lcd.setColor(36, 182, 255); // calipso
    //my_lcd.fillRoundRect(B2_X, B2_Y, B2_W, B2_H);
    my_lcd.setTextSize(3);
    my_lcd.setTextColor(WHITE);
    my_lcd.Print_String("MODE", B2_TXTX, B2_TXTY);
    #if SERIAL_DEBUG == true
    Serial.println("..B2 released");
    #endif
    sel = 2;
  }
  if ((X > B3_X && X < B3_W ) && (Y > B3_Y && Y < B3_H ) && b3_flag == LOW) {
    b3_flag = HIGH;
    #if SERIAL_DEBUG == true
    Serial.print("B3 pressed.. ");
    #endif
    //cambia color
    my_lcd.Set_Text_Back_colour(178,82,161);
    my_lcd.setTextSize(3);
    my_lcd.setTextColor(BLACK);
    my_lcd.Print_String("-", B3_TXTX, B3_TXTY);
    while (readResistiveTouch() == PRESSED) {

    }
    // vuelve al color original
    my_lcd.Set_Text_Back_colour(178,82,161);
    my_lcd.setTextColor(WHITE);
    my_lcd.Print_String("-", B3_TXTX, B3_TXTY);
    #if SERIAL_DEBUG == true
    Serial.println("B3 released.. ");
    #endif
    sel = 3;
  }
  if ((X > B4_X && X < B4_W ) && (Y > B4_Y && Y < B4_H ) && b4_flag == LOW) {
    b4_flag = HIGH;
    #if SERIAL_DEBUG == true
    Serial.print("B4 pressed.. ");
    #endif
    //Serial.print("\tz: ");
    //Serial.println(Z);
    // cambia de color el boton
    //my_lcd.setColor(0, 100, 0); //magenta
    //my_lcd.fillRoundRect(B4_X, B4_Y, B4_W, B4_H);
    my_lcd.setTextSize(3);
    my_lcd.Set_Text_Back_colour(69,95,101);
    my_lcd.setTextColor(BLACK);
    my_lcd.Print_String("START", B4_TXTX, B4_TXTY);
    while (readResistiveTouch() == PRESSED) {

    }
    //my_lcd.fillRoundRect(B4_X, B4_Y, B4_W, B4_H);
    my_lcd.setTextColor(WHITE);
    my_lcd.Set_Text_Back_colour(69,95,101);
    my_lcd.Print_String("START", B4_TXTX, B4_TXTY);
    #if SERIAL_DEBUG == true
    Serial.println("..B4 released");
    #endif
    sel = 4;
  }
  if ((X > B5_X && X < B5_W ) && (Y > B5_Y && Y < B5_H ) && b5_flag == LOW) {
    b5_flag = HIGH;
    #if SERIAL_DEBUG == true
    Serial.print("B5 pressed.. ");
    #endif
    //my_lcd.fillRoundRect(B5_X, B5_Y, B5_W, B5_H);
    my_lcd.setTextSize(3);
    my_lcd.setTextColor(BLACK);
    my_lcd.Set_Text_Back_colour(RED);
    my_lcd.Print_String("STOP", B5_TXTX, B5_TXTY);
    while (readResistiveTouch() == PRESSED) {

    }
    my_lcd.setTextColor(WHITE);
    my_lcd.Set_Text_Back_colour(RED);
    my_lcd.Print_String("STOP", B5_TXTX, B5_TXTY);
    #if SERIAL_DEBUG == true
    Serial.println("..B5 released");
    #endif
    sel = 5;
  }
  return sel;
}

void dibujarMenu() {

  //my_lcd.fillRoundRect(desde x , desde y, hasta x , hasta y );

  // boton 1
  my_lcd.setColor(178,82,161);
  my_lcd.fillRoundRect(B1_X, B1_Y, B1_W, B1_H);
  my_lcd.Set_Text_Back_colour(178,82,161);
  my_lcd.setTextSize(3);
  my_lcd.setTextColor(WHITE);
  my_lcd.Print_String("+", B1_TXTX,B1_TXTY);

  // boton 2
  my_lcd.setColor(44,4,82);
  my_lcd.fillRoundRect(B2_X, B2_Y, B2_W, B2_H);
  my_lcd.setTextColor(WHITE);
  my_lcd.Set_Text_Back_colour(44,4,82);
  my_lcd.Print_String("MODE", B2_TXTY, B2_TXTY);

  // boton 3
  my_lcd.setColor(178,82,161);
  my_lcd.fillRoundRect(B3_X, B3_Y, B3_W, B3_H);
  my_lcd.Set_Text_Back_colour(178,82,161);
  my_lcd.setTextColor(WHITE);
  my_lcd.Print_String("-", B3_TXTX, B3_TXTY);

  // boton 4 start
  my_lcd.setColor(69,95,101);
  my_lcd.fillRoundRect(B4_X, B4_Y, B4_W, B4_H);
  my_lcd.setTextSize(3);
  my_lcd.Set_Text_Back_colour(69,95,101);
  my_lcd.setTextColor(WHITE);
  my_lcd.Print_String("START", B4_TXTX, B4_TXTY);

  // boton 5 stop
  my_lcd.setColor(255, 0, 0);
  my_lcd.fillRoundRect(B5_X, B5_Y, B5_W, B5_H);
  my_lcd.setTextSize(3);
  my_lcd.Set_Text_Back_colour(RED);
  my_lcd.setTextColor(WHITE);
  my_lcd.Print_String("STOP", B5_TXTX, B5_TXTY);

  my_lcd.Set_Text_Back_colour(BLACK);
  // actual value
  my_lcd.setTextSize(3);
  my_lcd.setTextColor(WHITE);
  if (mode == TEMP) my_lcd.Print_String("dTEMP +-50", TZ_X + MARGIN, TZ_Y + MARGIN);
  // value
  my_lcd.setTextSize(3);
  my_lcd.setTextColor(WHITE);
  //my_lcd.Print_Number_Int(0, TZ_X + MARGIN, TZ_Y + MARGIN * 4,0, ' ',10);

  // time
  my_lcd.setTextSize(2);
  my_lcd.setTextColor(WHITE);
  my_lcd.Print_String("Ref:  ", REF_X, REF_Y);
  // time value
  my_lcd.setTextSize(3);
  my_lcd.setTextColor(WHITE);
  my_lcd.Print_Number_Int(tref, REF_X + DELTA_X, REF_Y, 0, ' ',10);

  // angle
  my_lcd.setTextSize(2);
  my_lcd.setTextColor(WHITE);
  my_lcd.Print_String("Temp: ", TEMP_X, TEMP_Y);
  // angle value
  my_lcd.setTextSize(3);
  my_lcd.setTextColor(WHITE);
  my_lcd.Print_Number_Int(temperatura, TEMP_X + DELTA_X, TEMP_Y,0, ' ',10);

    //my_lcd.setColor(64, 64, 64);
  //my_lcd.fillRect(0, 226, 319, 239);
  //my_lcd.setColor(255, 255, 255);
  //my_lcd.setBackColor(255, 0, 0);
  //my_lcd.print("* Universal Color TFT Display Library *", CENTER, 1);
  //my_lcd.setBackColor(64, 64, 64);
  //my_lcd.setColor(255,255,0);
  //my_lcd.print("<http://electronics.henningkarlsen.com>", CENTER, 227);

}

void updateMode() {

  my_lcd.Set_Text_Back_colour(BLACK);
  my_lcd.setTextSize(3);
  my_lcd.setTextColor(WHITE);
  my_lcd.Print_String("           ", TZ_X + MARGIN, TZ_Y + MARGIN);
  if (mode == TEMP) my_lcd.Print_String("dTEMP +-50", TZ_X + MARGIN, TZ_Y + MARGIN);
  if (mode == DTEMP) my_lcd.Print_String("dTEMP +-10", TZ_X + MARGIN, TZ_Y + MARGIN);
  if (mode == DDTEMP) my_lcd.Print_String("dTEMP +-1", TZ_X + MARGIN, TZ_Y + MARGIN);

}

void updateTemp() {

  my_lcd.Set_Text_Back_colour(BLACK);
  my_lcd.setTextSize(3);
  my_lcd.setTextColor(WHITE);
  my_lcd.Print_String("     ", TEMP_X + DELTA_X, TEMP_Y+DELTA_Y);
  my_lcd.Print_Number_Int((int)temperatura, TEMP_X + DELTA_X, TEMP_Y+DELTA_Y, 0,' ',10);

  my_lcd.setTextSize(3);
  my_lcd.Print_String("           ", STATUS_X, STATUS_Y);
  //my_lcd.setTextColor(WHITE);
  my_lcd.Print_String(en_st?"CALENTANDO":"APAGADO", STATUS_X, STATUS_Y);

}

void updateTRef() {

  my_lcd.Set_Text_Back_colour(BLACK);
  my_lcd.setTextSize(3);
  my_lcd.setTextColor(WHITE);
  my_lcd.Print_String("   ", REF_X + DELTA_X, REF_Y+DELTA_Y);
  my_lcd.Print_Number_Int(tref, REF_X + DELTA_X, REF_Y+DELTA_Y, 0, ' ',10);

}

void touchB1() {
  if (mode == TEMP) {
    tref += dtemp;
  }
  else if (mode == DTEMP) {
    tref += ddtemp;
  }
  else if (mode == DDTEMP) {
    tref += dddtemp;
  }

}

void touchB2() {
  if (mode == TEMP) {
    mode = DTEMP;
  }
  else if (mode == DTEMP) {
    mode = DDTEMP;
  }
  else if (mode == DDTEMP) {
    mode = TEMP;
  }
}

void touchB3() {
  if (mode == TEMP) {
    tref-=dtemp;
  }
  else if (mode == DTEMP) {
    tref-=ddtemp;
  }
  else if (mode == DDTEMP) {
    tref-=dddtemp;
  }
}

void touchB4() {
  //Serial.println("START");
  en_st = 1;
  //digitalWrite(M_EN, !en_st);
}

void touchB5() {
  //Serial.println("STOP");
  en_st = 0;
  //digitalWrite(M_EN, !en_st);
}
